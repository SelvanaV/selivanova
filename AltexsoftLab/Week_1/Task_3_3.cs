﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltexsoftLab.Week_1
{

    //Для данной директории рекурсивно выдать список её файлов и поддиректорий.
    namespace Task_3_3
    {
        public class Program 
        {
            public static void Execute(DirectoryInfo directory)
            {
                DirectoryTree(directory);
            }

            public static void DirectoryTree(DirectoryInfo directory)
            {
                Console.WriteLine(directory.FullName);

                DirectoryInfo[] directories = directory.GetDirectories();

                foreach(DirectoryInfo di in directories)
                {
                    DirectoryTree(di);
                }

                FileInfo[] files = directory.GetFiles();

                foreach(FileInfo file in files)
                {
                    Console.WriteLine(file.FullName);
                }
            }
        }

    }
}
