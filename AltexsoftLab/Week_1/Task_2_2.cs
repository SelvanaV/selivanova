﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltexsoftLab.Week_1
{
    //Для структуры Student создать массив. Отсортировать массив по полю FirstName.
   // a.Используя перегруженный метод Sort(IComparer), реализовать сортировку по полю Age.

    namespace Task_2_2
    {
        class Program
        {
            public static void Execute()
            {
                Student[] students =
                {
                    new Student { FirstName = "Алина", LastName = "Волкова", Age = 20},
                    new Student { FirstName = "Анатолий", LastName = "Сидоров", Age = 18},
                    new Student { FirstName = "Пет", LastName = "Петров", Age = 23},
                    new Student { FirstName = "Марк", LastName = "Антонов", Age = 25},
                    new Student { FirstName = "Полина", LastName = "Максимова", Age = 19},
                    new Student { FirstName = "Дмитрий", LastName = "Маликов", Age = 19}
                };

                Console.WriteLine("List of students: ");
                foreach(Student student in students)
                {
                    Console.WriteLine(student.ToString());
                }



                Array.Sort(students);
                Console.WriteLine();
                Console.WriteLine("Отсортированный список студентов по имени");

                foreach (Student student in students)
                {
                    Console.WriteLine(student.ToString());
                }

                Array.Sort(students, new StudentAgeCompare());
                Console.WriteLine();
                Console.WriteLine("Отсортированный список студентов по возрасту");

                foreach (Student student in students)
                {
                    Console.WriteLine(student.ToString());
                }
            }
        }


        struct Student : IComparable<Student>
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public int Age { get; set; }

            public int CompareTo(Student other)
            {
                return FirstName.CompareTo(other.FirstName);
            }

            public override string ToString()
            {
                return  string.Format("{0} {1}, {2} лет", FirstName, LastName, Age);
            }
        }

        class StudentAgeCompare : IComparer<Student>
        {
            public int Compare(Student x, Student y)
            {
                return x.Age - y.Age;
            }
        }
    }
}
