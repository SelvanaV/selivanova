﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltexsoftLab.Week_1
{
    //Реализовать класс для организации работы со списком студентов группы, включив следующие данные: ФИО, год рождения, домашний адрес, какую школу окончил.
    //Вывести информацию о студентах, окончивших заданную школу, отсортировав их по году рождения
    namespace Task_4_1
    {
        public class Program
        {
            public static void Execute()
            {
                Student[] students = new Student[]
                {
                    new Student { Fio = "Волкова Алина Марковна", School = "ЗОШ №4", YearOfBirth = 1993, Adress = "ул. Ленина 22, 170" },
                    new Student { Fio = "Сидоров Анатолий Анатолиевич", School = "ЗОШ №3", YearOfBirth = 1992, Adress = "ул. Мира 10,29" },
                    new Student { Fio = "Петров Петр Валериевич", School = "ЗОШ №5", YearOfBirth = 1993, Adress = "ул. ПУшкина 16,2" },
                    new Student { Fio = "Антонов Марк Антонович", School = "ЗОШ №4", YearOfBirth = 1995, Adress = "ул. Мира 54,9" },
                    new Student { Fio = "Павлова Елена Петровна", School = "ЗОШ №1", YearOfBirth = 1993, Adress = "ул. Ленина 31,72" },
                    new Student { Fio = "Друзь Виталина Бенедиктовна", School = "ЗОШ №4", YearOfBirth = 1991, Adress = "ул. Котлова 12,43" },
                    new Student { Fio = "Волков Евгений Петрович", School = "ЗОШ №4", YearOfBirth = 1993, Adress = "ул. Маркса 7, 52" },
                };

                Array.Sort(students, new YearStudentComparer());

                foreach (Student student in students)
                {
                    if (student.School.Equals("ЗОШ №4"))
                    {
                        Console.WriteLine(student);
                    }
                }

            }
        }

        public class Student
        {
            public string Fio { get; set; } 
            public int YearOfBirth { get; set; }
            public string School { get; set; }
            public string Adress { get; set; }
            public override string ToString()
            {
                return  string.Format("{0}, {1} лет, {2}, {3}", Fio, YearOfBirth, School, Adress);
                
            }


        }

        public class YearStudentComparer : IComparer<Student>
        {
            public int Compare(Student x, Student y)
            {
                return x.YearOfBirth - y.YearOfBirth;
 
            }
        }
    }


}
