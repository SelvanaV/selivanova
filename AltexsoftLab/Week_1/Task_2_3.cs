﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltexsoftLab.Week_1
{
    //	Дан одномерный массив размерностью N. Необходимо заполнить его случайными числами в диапазоне от -500 до 500 и отсортировать
    namespace Task_2_3
    {
        class Program
        {
            public static void Execute()
            {
                int[] array = new int[10];

                ArrayFill(array);

                foreach(int i in array )
                {
                    Console.Write("<{0}>", i);
                }

                Array.Sort(array);
                Console.WriteLine();
                foreach (int i in array)
                {
                    Console.Write("<{0}>", i);
                }


            }

            public static void ArrayFill(int[] arr)
            {
                Random r = new Random();
                for (int i = 0; i < arr.Length; i++)
                {
                    arr[i] = r.Next(-500, 500);
                }
            }
        }
        
    }
}
